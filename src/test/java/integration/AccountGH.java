package integration;

import com.codeborne.selenide.Configuration;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class AccountGH {
    @Test
    public void userLogin() {
        //CASE3
        //Шлях до файла chromedriver.exe
        System.setProperty("webdriver.chrome.driver", "C:/Users/Lenovo/Downloads/chromedriver_win32/chromedriver.exe");
        //Веб-сторінку автоматично відкриватиме в Google Chrome
        Configuration.browser = "chrome";


        open("https://github.com/login");
        //Вводимо свій логін й пароль.
        $("#login_field").setValue("selenideexmpl@gmail.com");
        $("#password").setValue("qaz1wsx2edc3rfv4tgb5");
        //Заходимо на свій аккаунт
        $(By.xpath("//input[@name='commit']")).click();
        //Створюємо новий проект
        $(By.xpath("//a[text()='Start a project']")).click();
        //Вводимо назву свого репозиторія
        $(By.xpath("//input[@name='repository[name]']")).setValue("SelenideExample");
        //Створюємо репозиторий
        $(By.xpath("//button[@class='btn btn-primary first-in-line']")).click();
        //Заходимо в Setting
        $(By.xpath("//a[@class='js-selected-navigation-item reponav-item'][4]")).click();
        //Прокручуємо вниз й натискаємо на кнопку "Delete this repository"
        $(By.xpath("//summary[contains(@class, 'btn-danger') and contains(@class, 'boxed-action') and contains(text(),'Delete this repository')]")).scrollTo().click();
    }
}
