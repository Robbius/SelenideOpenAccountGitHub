package integration;

import com.codeborne.selenide.Configuration;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class SearchKeyword {
    @Test
    public void userCanSearchKeywordWithGitHub() {
        //CASE 1
        //Шлях до файла chromedriver.exe
        System.setProperty("webdriver.chrome.driver", "C:/Users/Lenovo/Downloads/chromedriver_win32/chromedriver.exe");
        //Веб-сторінку автоматично відкриватиме в Google Chrome
        Configuration.browser = "chrome";

        //Відкриває GitHub й вводить ключове слово "Selenide"
        open("https://github.com");
        $(By.name("q")).setValue("Selenide").pressEnter();
        $$("#ires li.g").shouldHave(size(10));
        $("#ires li.g").shouldHave(text("codeborne/selenide"));

        //Перевіряємо текст в найденому репозиторії
        $(".f6 text-gray mr-3 mb-0 mt-2").shouldHave(text("MIT license"));
        $(".col-9 d-inline-block text-gray mb-2 pr-4").shouldHave(text("Concise UI Tests with Java!"));
        $(".d-table-cell col-2 text-gray pt-2").shouldHave(text("Java"));

        findButtonAndClick();
    }

    public void findButtonAndClick() {
        //Шукаємо й натискаємо на кнопку сортування
        $(By.xpath("//button[@class='btn btn-sm select-menu-button js-menu-target']")).click();
        //Вибираємо тип сортування "Most stars"
        $(By.xpath("//*[text()='Most stars']")).click();
    }
}

